let isMobActive = 'false';

const tara = [
  {
      img: "img/dad.jpg", name: "Dad", sentmessage: "Pranaam", time: "10:20 AM",
      chat: "Namashkar", status: "online", chatsent: "Pranaam"
  },
  {
      img: "img/group-dp.png", name: "LFP_NCT jun-aug batch 3", sentmessage: "Anshika: Koi hai kya, class me?", time: "10:20 AM",
      chat: "Anshika: Koi hai kya, class me?", status: "Nikhil, Pankaj, Pranav, Saiprasad, Sagar, ......", chatsent: "me hu 🙋🏼‍♂️"
  },
  {
      img: "img/sagar.jpg", name: "Sagar NCT", sentmessage: "https://www.bulletbonanza.io", time: "10:20 AM",
      chat: "Namashkar", status: "last seen today at 10:20 AM", chatsent: "https://www.bulletbonanza.io/?arenaID=6eedd711-b8cf-43f6-8ff2-da2608fffda4"
  },

  {
      img: "img/shailendra.jpg", name: "Shailendra", sentmessage: "😂", time: "10:20 AM",
      chat: "@bulla.meme pe kya reaction dega?", status: "last seen today at 10:20 AM", chatsent: "😂"
  },

  {
      img: "img/jaineek.jpg", name: "Jaineek", sentmessage: "Mast hai yaar", time: "10:20 AM",
      chat: "https://www.instagram.com/p/CiFKjwELzDt/", status: "last seen today at 10:20 AM", chatsent: "Mast hai yaar"
  },

  {
      img: "img/karan.jpg", name: "Karan R", sentmessage: "https://store.epicgames.com", time: "10:20 AM",
      chat: "Koi Game Free mil raha hai kya?", status: "last seen today at 10:20 AM", chatsent: "https://store.epicgames.com/en-US/p/shadow-of-the-tomb-raider"
  },

  {
      img: "img/pranav.jpg", name: "Pranav Sir", sentmessage: "https://youtu.be/y0y7wJJSTR4", time: "10:20 AM",
      chat: "Koi free non copyright gameplay hai kya?", status: "last seen today at 10:20 AM", chatsent: "https://youtu.be/y0y7wJJSTR4"
  },

  {
      img: "img/anshika.jpg", name: "Anshika Jaiswal LFP", sentmessage: "hello", time: "10:20 AM",
      chat: "hii", status: "online", chatsent: "hello"
  },

  {
      img: "img/pankaj.jpg", name: "Pankaj Sir Ji", sentmessage: "Jai Hind Sir Ji", time: "10:20 AM",
      chat: "Jai Hind Tara", status: "last seen today at 10:20 AM", chatsent: "Jai Hind Sir Ji"
  },

  {
      img: "img/gaon.jpg", name: "फतेहपुर अन्नदाता 🌽🍅🍏🍆🧄", sentmessage: "Koi scheme hai kya kisano ke liye?", time: "10:20 AM",
      chat: "Admin: Ek khushkhabaar hai!!", status: "Walu, अबालाल, जगनाथ, भेरू, .........", chatsent: "Koi scheme hai kya kisano ke liye?"
  },

  {
      img: "img/outscal.jpg", name: "Launchpad by Outscal 🚀 3A", sentmessage: "I can do it!!!", time: "10:20 AM",
      chat: "Become a Game Developer with us!", status: "click here for group info", chatsent: "I can do it!!!"
  },

  {
      img: "img/camelot.jpg", name: "Cʟᴀsʜ Oɴ Cᴀᴍᴇʟᴏᴛ", sentmessage: "Donations Please", time: "10:20 AM",
      chat: "Rick Sir Ji: Guys Attack in War", status: "Black, Pritam, Rick, .........", chatsent: "Pranaam"
  },

  {
      img: "img/aniket.jpg", name: "Aniket LFP", sentmessage: "Yeh jagah mujhe dede aniket", time: "10:20 AM",
      chat: "Hello Tara", status: "last seen today at 03:45 PM", chatsent: "Yeh jagah mujhe dede aniket"
  },

  {
      img: "img/sonu.jpg", name: "Sonu Didi", sentmessage: "😜", time: "10:20 AM",
      chat: "Kidar hai? Ghar kab taak aa raha hai?", status: "last seen today at 03:45 PM", chatsent: "😜"
  },

  {
      img: "img/niyati.jpeg", name: "Niyati", sentmessage: "Wow! Kya sahi idea hai. Jarur", time: "10:20 AM",
      chat: "School Reunion ka plan banaye?", status: "last seen today at 03:45 PM", chatsent: "Wow! Kya sahi idea hai. Jarur"
  },

  {
      img: "img/hira.jpg", name: "Hira Bhaiya", sentmessage: "Haa hai na 'Simplest stock picking strategy'", time: "10:20 AM",
      chat: "Koi nayi book padhne ke liye hai kya?", status: "last seen today at 03:45 PM", chatsent: "Haa hai na 'Simplest stock picking strategy'"
  },
];

// A constant which is assigned by a array that 'tara.map()' will return
// map() method runs a speicifc method for every element of an array, in this case 'tara'
const sidebarChats = tara.map((chat) => `
  <div class="sidebar-chat" onclick="myfunction(${tara.indexOf(chat)})">
    <div class="chat-avatar">
      <img src="${chat.img}" alt="${chat.name} chat dp">
    </div>
    <div class="chat-info">
      <h4>${chat.name}</h4>
      <p>${chat.sentmessage}</p>
    </div>
    <div class="time">
      <p>${chat.time}</p>
    </div>
  </div>
`).join("");//now the 'join()' method joins an array with specified 'seprator' in our case double inverted commas and all these is stored in
// 'sidebarChats'

// now we change the innerHTML of sidebar-chats with 'sidebarChats'
document.getElementById("sidebar-chats").innerHTML = sidebarChats;

// I declared a function to change the background of the message container
function bgChange() {
// a constant named 'defaultBg' is stored by the content of 'message-content'
  const defaultBg = document.getElementById('message-content');

  // the cssText property changes multiple css properties at once
  defaultBg.style.cssText = `
    position: static;
    z-index: 1;
    width: auto;
    height: calc(100vh - 115px);
    background-image: url('default-bg.jpg');
    background-repeat: no-repeat;
  `;
}


function myfunction(selected) {
  const clickChange = (index) => (selected === index ? "sidebar-chat onclick" : "sidebar-chat");

  // here the map() method runs this function for every element of the array named 'tara' and the functions 1st parameter is the element that is in the cycle i.e 'chat' and 2nd parameter is the index of the element in the cycle i.e 'i'
  const sidebarChatsHTML = tara.map((chat, i) => `
    <div class="${clickChange(i)}" onclick="myfunction(${i})">
      <div class="chat-avatar">
        <img src="${chat.img}" alt="">
      </div>
      <div class="chat-info">
        <h4>${chat.name}</h4>
        <p>${chat.sentmessage}</p>
      </div>
      <div class="time">
        <p>${chat.time}</p>
      </div>
    </div>
  `).join("");//now the 'join()' method joins an array with specified 'seprator' in our case double inverted commas and all these is stored in
  // 'sidebarChats'

  // 'tara[selected]' will return the specific chat from the 'tara' named array, 'selected' is the index of the element that is in the cycle in the 'map()' method
  const selectedChat = tara[selected];

  document.getElementById('sidebar-chats').innerHTML = sidebarChatsHTML;

  document.getElementById("header").innerHTML = `
    <div class="chat-title">
    <img src="left-arrow.svg" alt="" class="logo back-btn" id="mob-back-btn" onclick="backButton()">
      <div class="avatar" id="mobAvt">
        <img src="${selectedChat.img}" alt="">
      </div>
      <div class="message-header-content">
        <h4>${selectedChat.name}</h4>
        <p>${selectedChat.status}</p>
      </div>
    </div>
    <div class="chat-header-right" id="mobRgt">
      <img src="search.svg" alt="" class="logo">
      <img src="more.svg" alt="" class="logo">
    </div>
  `;

  document.getElementById("message-content").innerHTML = `
    <p class="chat-message">${selectedChat.chat}<span class="chat-timestamp">${selectedChat.time}</span></p>
    <p class="chat-message chat-sent">${selectedChat.chatsent}<span class="chat-timestamp">${selectedChat.time}</span></p>
  `;

  if (window.innerWidth <= "767") {
    isMobActive = 'true';
    document.getElementById("sidebar").style.display = "none";
    document.getElementById("message-container").style.display = "block";

  } else {
    document.getElementById("sidebar").style.display = "block";
    document.getElementById("message-container").style.display = "block";
  }
}


var searchSvg = document.getElementById('input-svg');

function inputBack() {
  if (searchSvg.src.includes("search.svg")) {
    searchSvg.src = "left-arrow.svg";
  } else {
    searchSvg.src = "search.svg";
  }
}

const backButton = () => {
  if (isMobActive == 'true') {
    document.getElementById("sidebar").style.display = "block";
    document.getElementById("message-container").style.display = "none";
  }
}
